import axios from "axios"

const url = "https://jsonplaceholder.typicode.com/todos/"

const url2 = "https://jsonplaceholder.typicode.com/users/"

try {
  const response = await axios.get(url)
  console.log(response.data)
}
catch (error) {
  console.error(error.message)
}

/*
try {
    const response = await axios.get(url2)
    console.log(response.data)
}
catch (error) {
    console.error(error.message)
}*/


/*
funktio = async () => {
    let promise = new Promise((resolve, reject) => {
        setTimeout(() => resolve("done!"), 1000)
    });
    let result = await promise; // wait until the promise resolves
    console.log(result);
}

funktio();



let delay = (time, someStr) => new Promise((res, rej) => {
    setTimeout(() => res(someStr), time);
});

let assy = async () => {
    console.log( await delay(1000, "first") );
    console.log( await delay(2000, "second") );
    console.log( await delay(500, "third") );
    console.log( await delay(1500, "fourth") );
}

assy();
*/